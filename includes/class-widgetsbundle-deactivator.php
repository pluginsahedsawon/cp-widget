<?php

/**
 * Fired during plugin deactivation
 *
 * @link       inserttech.com/sahed.sawon
 * @since      1.0.0
 *
 * @package    Widgetsbundle
 * @subpackage Widgetsbundle/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Widgetsbundle
 * @subpackage Widgetsbundle/includes
 * @author     sahed sawon <plugin.sahed.sawon@gmail.com>
 */
class Widgetsbundle_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

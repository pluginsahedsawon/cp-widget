<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       inserttech.com/sahed.sawon
 * @since      1.0.0
 *
 * @package    Widgetsbundle
 * @subpackage Widgetsbundle/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Widgetsbundle
 * @subpackage Widgetsbundle/includes
 * @author     sahed sawon <plugin.sahed.sawon@gmail.com>
 */
class Widgetsbundle_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'widgetsbundle',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

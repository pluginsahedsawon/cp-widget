<?php
class Widgetbundle  extends WP_Widget{
	function __construct() {
		// Instantiate the parent object
		parent::__construct( 
			'popular_post', //Base Id	
			__('popular posts'), // Name of the widget
			 array(	'description' => __('popular posts of your blog') ) // Args		
			 );
	}

	function widget( $args, $instance ) {
		// Widget output
		echo $args['before_widget'];

		if ( !empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		} else {
			echo $args['before_title'] . apply_filters( 'widget_title', 'Popular posts' ) . $args['after_title'];
		}
		$defaultargs  = array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'order'		=> 'DESC',
			'orderby'   => 'comment_count',
			'posts_per_page' => 7,
			);
		$post_type = !empty( $instance['post_type'] )?  $instance['post_type'] :array('post');

		$args = array(
			'post_type' => $post_type,
			'post_status' => 'publish',
			'order'		=> $instance["order"],
			'orderby'   => $instance["orderBy"],
			'posts_per_page' => intval($instance["Nuber_of_posts"]),
			);
		$finalargs = wp_parse_args($args,$defaultargs);
		echo '<div class="color-calander widget-body-container" style="margin-bottom:4.307692308em;background:#'.$instance['body_background_color'].'">';
		// the query
		require_once plugin_dir_path(dirname(__FILE__)).'public/partials/cp_calendar.php';
		$column_style;
		switch ($instance['layout'] ) {
			case 'single':
				$column_style = single_column_Post_widget($finalargs,$instance);
				break;
			case 'two':
				$column_style = two_column_Post_widget($finalargs,$instance);	
				break;
			case 'simplelist':
				$column_style = simple_list_style($finalargs,$instance);
				break;
			case 'minimallist':
				$column_style = minimal_list_style($finalargs,$instance);
				break;
			case 'clasiccoverstyle':
				$column_style = clasiccoverstyle($finalargs,$instance);
				break;
			case 'moderncoverstyle':
				$column_style =  moderncoverstyle($finalargs,$instance);
				break;
			case 'cleancoverstyle':
				$column_style =  cleancoverstyle($finalargs,$instance);
				break;
				
			case 'simplegrid':
				$column_style = simple_grid_style($finalargs,$instance);
				?>
					<script type="text/javascript">
					(function( $ ) {
					'use strict';
						$(function() {
						$(".widget_popular_post").children("h2.widget-title").css("textAlign","center");
						});
					})( jQuery );
					</script>
				<?php
				break;
			
			default:
				$column_style = single_column_Post_widget($finalargs,$instance);
				break;
		}
		echo $column_style;
		
		echo '</div>';
		echo $args['after_widget'];
	}
	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['orderBy'] = ( ! empty( $new_instance['orderBy'] ) ) ? strip_tags( $new_instance['orderBy'] ) : '';
		$instance['order'] = ( ! empty( $new_instance['order'] ) ) ? strip_tags( $new_instance['order'] ) : '';
		$instance['Nuber_of_posts'] = ( ! empty( $new_instance['Nuber_of_posts'] ) ) ? strip_tags( $new_instance['Nuber_of_posts'] ) : '';
		
		$instance['post_type'] = ( ! empty( $new_instance['post_type'] ) ) ? strip_tags( $new_instance['post_type'] ) : '';
		$instance['show_comment_count'] = ( ! empty( $new_instance['show_comment_count'] ) ) ? strip_tags( $new_instance['show_comment_count'] ) : '';
		$instance['layout'] = ( ! empty( $new_instance['layout'] ) ) ? strip_tags( $new_instance['layout'] ) : '';
		$instance['body_background_color'] = !empty( $new_instance['body_background_color'] )? strip_tags( $new_instance['body_background_color']):'';
		$instance['title_color'] = !empty( $new_instance['title_color'] )? strip_tags( $new_instance['title_color']):'';
		$instance['post_date_color'] = !empty( $new_instance['post_date_color'] )? strip_tags( $new_instance['post_date_color']):'';
		$instance['comment_box_color'] = !empty( $new_instance['comment_box_color'] )? strip_tags( $new_instance['comment_box_color']):'';
		$instance['comment_box_background_color'] = !empty( $new_instance['comment_box_background_color'] )? strip_tags( $new_instance['comment_box_background_color']):'';
		
		$instance['post_separator_botom_border_color'] = !empty( $new_instance['post_separator_botom_border_color'] )? strip_tags( $new_instance['post_separator_botom_border_color']):'';
		$instance['Thumbnail_height'] = !empty( $new_instance['Thumbnail_height'] )?strip_tags( $new_instance['Thumbnail_height'] ) : '';
		$instance['author_color'] = !empty( $new_instance['author_color'] )?strip_tags( $new_instance['author_color'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Popular Posts', 'text_domain' );
		$post_type = ! empty( $instance['post_type'] ) ? $instance['post_type'] :'post';
		$orderByopt = ! empty( $instance['orderBy'] ) ? $instance['orderBy'] : __( 'comment_count', 'text_domain' );
		$orderopt = ! empty( $instance['order'] ) ? $instance['order'] : __( 'DESC', 'text_domain' );
		$Nuber_of_posts = ! empty( $instance['Nuber_of_posts'] ) ? $instance['Nuber_of_posts'] : __( 5, 'text_domain' );
		$show_comment_count = !empty($instance['show_comment_count'] )? 'checked="checked"' :'';
		$Thumbnail_height = !empty($instance['Thumbnail_height'])?$instance['Thumbnail_height']:'';
		$column_style = array(
			'single' 	=> 'single column',
			'two' 		=> 'two column',
			'simplelist' => 'simple list style',
			'minimallist' => 'minimal list style',
			'simplegrid' => 'Grid View Simple Style',
			'clasiccoverstyle' => 'Classic cover style',
			'moderncoverstyle' => 'Modern cover style',
			'cleancoverstyle'  => 'Clean cover style',
			);
		$body_background_color = !empty( $instance['body_background_color'] )?$instance['body_background_color']:'';
		$title_color = !empty( $instance['title_color'] )?$instance['title_color']:'';
		$post_date_color = !empty( $instance['post_date_color'] )?$instance['post_date_color']:'';
		$comment_box_color = !empty( $instance['comment_box_color'] )?$instance['comment_box_color']:'';
		$comment_box_background_color = !empty( $instance['comment_box_background_color'] )?$instance['comment_box_background_color']:'';
		$post_separator_botom_border_color = !empty( $instance['post_separator_botom_border_color'] )?$instance['post_separator_botom_border_color']:'';
		$author_color = !empty($instance['author_color'])? $instance['author_color']:'';

		$orderBys = array('none','ID','author','title','name','type','date','modified','parent','rand','comment_count');
		$orders  = array('ASC','DESC');
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
			value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'Thumbnail_height' ); ?>"><?php _e( 'Thumbnail height :' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'Thumbnail_height' ); ?>"
			name="<?php echo $this->get_field_name( 'Thumbnail_height' ); ?>" type="number" step="1" min="1" size="3" value="<?php echo esc_attr( $Thumbnail_height ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Layout' ); ?></label>
			<select  class="tiny-text" name="<?php echo $this->get_field_name( 'layout' ); ?>" id="<?php echo $this->get_field_id( 'layout' ); ?>">
				<?php
				foreach ($column_style as  $key =>$order) {
					?>
					<option value="<?php echo $key;?>"  <?php selected( $key, $instance['layout'] );?>  ><?php echo __(ucfirst($order)) ?></option>
					<?php
				}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'orderBy' ); ?>"><?php _e( 'Order By' ); ?></label>
			<select  class="tiny-text" name="<?php echo $this->get_field_name( 'orderBy' ); ?>" id="<?php echo $this->get_field_id( 'orderBy' ); ?>">
				<?php
				foreach ($orderBys as  $order) {
					?>
					<option value="<?php echo $order;?>"  <?php selected( $order, $orderByopt );?>  ><?php echo __(ucfirst($order)) ?></option>
					<?php
				}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post types:' ); ?></label>
			<select class="tiny-text" name="<?php echo $this->get_field_name( 'post_type' ); ?>" id="<?php echo $this->get_field_id( 'post_type' ); ?>">
				<?php
				foreach ( get_post_types() as  $odr) {
					if ( ('attachment' == $odr ) || ('revision' == $odr ) || ('nav_menu_item' == $odr ) ) {
						continue;
					}
					?>
					<option value="<?php echo $odr;?>"  <?php selected( $odr, $post_type );?>  ><?php echo __(ucfirst($odr)) ?></option>
					<?php
				}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order' ); ?></label>
			<select class="tiny-text" name="<?php echo $this->get_field_name( 'order' ); ?>" id="<?php echo $this->get_field_id( 'order' ); ?>">
				<?php
				foreach ($orders as  $odr) {
					?>
					<option value="<?php echo $odr;?>"  <?php selected( $odr, $orderopt );?>  ><?php echo __(ucfirst($odr)) ?></option>
					<?php
				}
				?>
			</select>
		</p>
		
		<p>
			<input class="checkbox" type="checkbox" <?php echo $show_comment_count; ?> id="<?php echo $this->get_field_id( 'show_comment_count' ); ?>" name="<?php echo $this->get_field_name( 'show_comment_count' ); ?>">
			<label for="<?php echo $this->get_field_id( 'show_comment_count' ); ?>"><?php echo __('Display comment count');?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'Nuber_of_posts' ); ?>"><?php echo __('Number of posts to show:')?></label>
			<input class="tiny-text" id="<?php echo $this->get_field_id( 'Nuber_of_posts' ); ?>" name="<?php echo $this->get_field_name( 'Nuber_of_posts' ); ?>" type="number" step="1" min="1" value="<?php echo esc_attr( $Nuber_of_posts ); ?>" size="3">
		</p>
		<div class="color-calander">
		<p>
			<label for="<?php echo $this->get_field_id( 'body_background_color' ); ?>"><?php echo __('Body background color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'body_background_color' ); ?>" name="<?php echo $this->get_field_name( 'body_background_color' ); ?>" type="text"  value="<?php echo esc_attr( $body_background_color ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'title_color' ); ?>"><?php echo __('Title color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'title_color' ); ?>" name="<?php echo $this->get_field_name( 'title_color' ); ?>" type="text"  value="<?php echo esc_attr( $title_color ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_date_color' ); ?>"><?php echo __('Post date color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'post_date_color' ); ?>" name="<?php echo $this->get_field_name( 'post_date_color' ); ?>" type="text"  value="<?php echo esc_attr( $post_date_color ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'comment_box_color' ); ?>"><?php echo __('Comment box color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'comment_box_color' ); ?>" name="<?php echo $this->get_field_name( 'comment_box_color' ); ?>" type="text"  value="<?php echo esc_attr( $comment_box_color ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'comment_box_background_color' ); ?>"><?php echo __('Comment box background color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'comment_box_background_color' ); ?>" name="<?php echo $this->get_field_name( 'comment_box_background_color' ); ?>" type="text"  value="<?php echo esc_attr( $comment_box_background_color ); ?>">
		</p>		
		<p>
			<label for="<?php echo $this->get_field_id( 'post_separator_botom_border_color' ); ?>"><?php echo __('Post separator botom border color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'post_separator_botom_border_color' ); ?>" name="<?php echo $this->get_field_name( 'post_separator_botom_border_color' ); ?>" type="text"  value="<?php echo esc_attr( $post_separator_botom_border_color ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'author_color' ); ?>"><?php echo __('Author color:')?></label>
			<input class="form-control widgetBoundlecolorpicker" id="<?php echo $this->get_field_id( 'author_color' ); ?>" name="<?php echo $this->get_field_name( 'author_color' ); ?>" type="text"  value="<?php echo esc_attr( $author_color ); ?>">
		</p>
		</div>
		<?php
	}
}

class cp_calendar  extends WP_Widget{
	function __construct() {
		// Instantiate the parent object
		parent::__construct( 
			'colorfull_post_calendar', //Base Id	
			__('Colorfull post calendar'), // Name of the widget
			 array(	'description' => __('A colorfull calendar of your site’s Posts.') ) // Args		
			 );
	}

	function widget( $args, $instance ) {
		// Widget output
		echo $args['before_widget'];

		if ( !empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		} else {
			echo $args['before_title'] . apply_filters( 'widget_title', 'Posts calendar' ) . $args['after_title'];
		}
		require_once plugin_dir_path(dirname(__FILE__)).'public/partials/cp_calendar.php';
		?>
		<div class="color-calander">
			<div class="row">
				<div class="col-xs-12">
					<div id="colorfull_post">
						<?php echo cp_calendar_genarator(2016,11);?>
					</div>
				</div>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}
	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		$title = !empty($instance['title']) ? $instance['title'] : __( 'Posts calendar', 'text_domain' );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
			value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}
}

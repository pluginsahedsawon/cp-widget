<?php

/**
 * Fired during plugin activation
 *
 * @link       inserttech.com/sahed.sawon
 * @since      1.0.0
 *
 * @package    Widgetsbundle
 * @subpackage Widgetsbundle/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Widgetsbundle
 * @subpackage Widgetsbundle/includes
 * @author     sahed sawon <plugin.sahed.sawon@gmail.com>
 */
class Widgetsbundle_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

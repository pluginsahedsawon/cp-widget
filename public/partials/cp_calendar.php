<?php
	function cp_calendar_genarator($thisyear,$thismonth,$initial = true)
	{
		global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;


	// week_begins = 0 stands for Sunday
	$week_begins = (int) get_option( 'start_of_week' );
	$ts = current_time( 'timestamp' );
	$unixmonth = mktime( 0, 0 , 0, $thismonth, 1, $thisyear );
	$last_day = date( 't', $unixmonth );

	// Get the next and previous month and year with at least one post
	$previous = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date < '$thisyear-$thismonth-01'
		AND post_type = 'post' AND post_status = 'publish'
			ORDER BY post_date DESC
			LIMIT 1");
	$next = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date > '$thisyear-$thismonth-{$last_day} 23:59:59'
		AND post_type = 'post' AND post_status = 'publish'
			ORDER BY post_date ASC
			LIMIT 1");

	/* translators: Calendar caption: 1: month name, 2: 4-digit year */
	$calendar_caption = _x('%1$s %2$s', 'calendar caption');
	$calendar_output = '<table id="cp-calendar">
	<caption>' . sprintf(
		$calendar_caption,
		$wp_locale->get_month( $thismonth ),
		date( 'Y', $unixmonth )
	) . '</caption>
	<thead>
	<tr>';

	$myweek = array();

	for ( $wdcount = 0; $wdcount <= 6; $wdcount++ ) {
		$myweek[] = $wp_locale->get_weekday( ( $wdcount + $week_begins ) % 7 );
	}

	foreach ( $myweek as $wd ) {
		$day_name = $initial ? $wp_locale->get_weekday_initial( $wd ) : $wp_locale->get_weekday_abbrev( $wd );
		$wd = esc_attr( $wd );
		$calendar_output .= "\n\t\t<th class=\"text-center\" scope=\"col\" title=\"$wd\">$day_name</th>";
	}

	$calendar_output .= '
	</tr>
	</thead>

	<tfoot>
	<tr>';

	if ( $previous ) {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev">'.date('M y',strtotime($previous->year.'-'.$previous->month)).'</td>';
	} else {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev" class="pad">&nbsp;</td>';
	}

	$calendar_output .= "\n\t\t".'<td class="pad">&nbsp;</td>';

	if ( $next ) {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="next"><a href="javascript:;">' .
		date('M y',strtotime($next->year.'-'.$next->month)).
		' &raquo;</a></td>';
	} else {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="next" class="pad">&nbsp;</td>';
	}

	$calendar_output .= '
	</tr>
	</tfoot>

	<tbody>
	<tr>';

	$daywithpost = array();

	// Get days with posts
	$dayswithposts = $wpdb->get_results("SELECT DISTINCT DAYOFMONTH(post_date)
		FROM $wpdb->posts WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00'
		AND post_type = 'post' AND post_status = 'publish'
		AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59'", ARRAY_N);
	if ( $dayswithposts ) {
		foreach ( (array) $dayswithposts as $daywith ) {
			$daywithpost[] = $daywith[0];
		}
	}

	// See how much we should pad in the beginning
	$pad = calendar_week_mod( date( 'w', $unixmonth ) - $week_begins );
	if ( 0 != $pad ) {
		$calendar_output .= "\n\t\t".'<td colspan="'. esc_attr( $pad ) .'" class="pad">&nbsp;</td>';
	}

	$newrow = false;
	$daysinmonth = (int) date( 't', $unixmonth );

	for ( $day = 1; $day <= $daysinmonth; ++$day ) {
		if ( isset($newrow) && $newrow ) {
			$calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
		}
		$newrow = false;
		$classname;
		

		if ( $day == gmdate( 'j', $ts ) &&
			$thismonth == gmdate( 'm', $ts ) &&
			$thisyear == gmdate( 'Y', $ts ) ) {

				if ( in_array( $day, $daywithpost ) ) {
						$calendar_output .= '<td class="have-post text-center" id="today" data-date="'.$thisyear.'-'.$thismonth.'-'.$day.'">' ;
				} else {
					$calendar_output .= '<td class="no-post text-center" id="today" data-date="'.$thisyear.'-'.$thismonth.'-'.$day.'">'; 
				} 

		} else {
				if ( in_array( $day, $daywithpost ) ) {
						$calendar_output .= '<td class="have-post text-center"  data-date="'.$thisyear.'-'.$thismonth.'-'.$day.'">' ;
				} else {
					$calendar_output .= '<td class="no-post text-center"  data-date="'.$thisyear.'-'.$thismonth.'-'.$day.'">'; 
				}
		}
		$calendar_output .= $day;
		$calendar_output .= '</td>';

		if ( 6 == calendar_week_mod( date( 'w', mktime(0, 0 , 0, $thismonth, $day, $thisyear ) ) - $week_begins ) ) {
			$newrow = true;
		}
	}

	$pad = 7 - calendar_week_mod( date( 'w', mktime( 0, 0 , 0, $thismonth, $day, $thisyear ) ) - $week_begins );
	if ( $pad != 0 && $pad != 7 ) {
		$calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr( $pad ) .'">&nbsp;</td>';
	}
	$calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table>";
	return $calendar_output;
	}
	function previousbutton($year,$month)
	{
		global $wpdb;
		$previous = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date < '$year-$month-01'
		AND post_type = 'post' AND post_status = 'publish'
			ORDER BY post_date DESC
			LIMIT 1");
		return $previous;
	}
	function nextbutton($year,$month)
	{
		global $wpdb;
		$date = $year.'-'.$month;
		$last_day = date('t',strtotime($date));
		$next = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date > '$year-$month-{$last_day} 23:59:59'
		AND post_type = 'post' AND post_status = 'publish'
			ORDER BY post_date ASC
			LIMIT 1");
		return $next;
	}

	function single_column_Post_widget($finalargs,$instance)
	{
		$content='';
		$the_query = new WP_Query( $finalargs );
		 if ( $the_query->have_posts() ) : 
				while ( $the_query->have_posts() ) : $the_query->the_post(); 
					
					$content .= '<div class="widget-container"';
					if (!empty( $instance['post_separator_botom_border_color'] ) ) {
						$content .= 'style="border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
					}else{
						$content .= '>';
					}
					
					$content .= '<div class="row">';
					$content .= '<div class="col-xs-12">';
					$content .= '<div class="widget-image-container"';
					if ( !empty($instance['Thumbnail_height'] )) {
						$content .='style="height:'.$instance['Thumbnail_height'].'px">';
					}else{
						$content .= '>';
					}
					$content .= '<img class="widget-img" src="'.wp_make_link_relative(get_the_post_thumbnail_url()).'">';
					$content .= '</div>';
					$content .= '</div>';
					$content .= '<div class="col-xs-12">';
					$content .= '<div class="widget-title-continer widget-title-alignment">';
					$content .= '<h4 style="margin-top:0px;margin-bottom:10px;">';
					$content .= '<a class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';
					if ( !empty($instance['title_color'] ) ) {
						$content .= ' style="color:#'.$instance['title_color'].'">'.get_the_title().'</a>';
					}else{
						$content .= '>'.get_the_title().'</a>';
					}
					
					if ( !empty($instance['show_comment_count'] ) ) {
						
						$style = '';
						if ( !empty($instance['comment_box_color'] ) ) {
							$style .= 'color:#'.$instance['comment_box_color'].';';
						}
						if ( !empty($instance['comment_box_background_color'] ) ) {
							$style .= 'background-color:#'.$instance['comment_box_background_color'].';';
						}
						$content .= '<span class="comment-color" style="'.$style.'">'.get_comments_number().'</span>';
					}
					$content .= '</h4>';
					$content .=  '<p  class="widget-time"';
					if (!empty( $instance['post_date_color'] ) ) {
						$content .= ' style="color:#'.$instance['post_date_color'].'">';
					}
					$content .= '<span style="vertical-align:text-bottom"><svg class="svg-icon" viewBox="0 0 20 20">
							<path fill="none" d="M16.254,3.399h-0.695V3.052c0-0.576-0.467-1.042-1.041-1.042c-0.576,0-1.043,0.467-1.043,1.042v0.347H6.526V3.052c0-0.576-0.467-1.042-1.042-1.042S4.441,2.476,4.441,3.052v0.347H3.747c-0.768,0-1.39,0.622-1.39,1.39v11.813c0,0.768,0.622,1.39,1.39,1.39h12.507c0.768,0,1.391-0.622,1.391-1.39V4.789C17.645,4.021,17.021,3.399,16.254,3.399z M14.17,3.052c0-0.192,0.154-0.348,0.348-0.348c0.191,0,0.348,0.156,0.348,0.348v0.347H14.17V3.052z M5.136,3.052c0-0.192,0.156-0.348,0.348-0.348S5.831,2.86,5.831,3.052v0.347H5.136V3.052z M16.949,16.602c0,0.384-0.311,0.694-0.695,0.694H3.747c-0.384,0-0.695-0.311-0.695-0.694V7.568h13.897V16.602z M16.949,6.874H3.052V4.789c0-0.383,0.311-0.695,0.695-0.695h12.507c0.385,0,0.695,0.312,0.695,0.695V6.874z M5.484,11.737c0.576,0,1.042-0.467,1.042-1.042c0-0.576-0.467-1.043-1.042-1.043s-1.042,0.467-1.042,1.043C4.441,11.271,4.908,11.737,5.484,11.737z M5.484,10.348c0.192,0,0.347,0.155,0.347,0.348c0,0.191-0.155,0.348-0.347,0.348s-0.348-0.156-0.348-0.348C5.136,10.503,5.292,10.348,5.484,10.348z M14.518,11.737c0.574,0,1.041-0.467,1.041-1.042c0-0.576-0.467-1.043-1.041-1.043c-0.576,0-1.043,0.467-1.043,1.043C13.475,11.271,13.941,11.737,14.518,11.737z M14.518,10.348c0.191,0,0.348,0.155,0.348,0.348c0,0.191-0.156,0.348-0.348,0.348c-0.193,0-0.348-0.156-0.348-0.348C14.17,10.503,14.324,10.348,14.518,10.348z M14.518,15.212c0.574,0,1.041-0.467,1.041-1.043c0-0.575-0.467-1.042-1.041-1.042c-0.576,0-1.043,0.467-1.043,1.042C13.475,14.745,13.941,15.212,14.518,15.212z M14.518,13.822c0.191,0,0.348,0.155,0.348,0.347c0,0.192-0.156,0.348-0.348,0.348c-0.193,0-0.348-0.155-0.348-0.348C14.17,13.978,14.324,13.822,14.518,13.822z M10,15.212c0.575,0,1.042-0.467,1.042-1.043c0-0.575-0.467-1.042-1.042-1.042c-0.576,0-1.042,0.467-1.042,1.042C8.958,14.745,9.425,15.212,10,15.212z M10,13.822c0.192,0,0.348,0.155,0.348,0.347c0,0.192-0.156,0.348-0.348,0.348s-0.348-0.155-0.348-0.348C9.653,13.978,9.809,13.822,10,13.822z M5.484,15.212c0.576,0,1.042-0.467,1.042-1.043c0-0.575-0.467-1.042-1.042-1.042s-1.042,0.467-1.042,1.042C4.441,14.745,4.908,15.212,5.484,15.212z M5.484,13.822c0.192,0,0.347,0.155,0.347,0.347c0,0.192-0.155,0.348-0.347,0.348s-0.348-0.155-0.348-0.348C5.136,13.978,5.292,13.822,5.484,13.822z M10,11.737c0.575,0,1.042-0.467,1.042-1.042c0-0.576-0.467-1.043-1.042-1.043c-0.576,0-1.042,0.467-1.042,1.043C8.958,11.271,9.425,11.737,10,11.737z M10,10.348c0.192,0,0.348,0.155,0.348,0.348c0,0.191-0.156,0.348-0.348,0.348s-0.348-0.156-0.348-0.348C9.653,10.503,9.809,10.348,10,10.348z"></path>
						</svg></span>';
					$content .= '<span style="padding-left:5px;vertical-align:text-top">'.get_the_date('d F Y').'</span></p>';
					
					$content .= '</div>';
					$content .= '</div>';
					$content .= '</div>';
					$content .= '</div>';
				endwhile; 
				wp_reset_postdata(); 
			else:
				$content .= 'Sorry, no posts matched your criteria.';
			endif; 
			return $content;
	}

	function two_column_Post_widget($finalargs,$instance)
	{
		$content='';
		$the_query = new WP_Query( $finalargs );
		 if ( $the_query->have_posts() ) : 
				while ( $the_query->have_posts() ) : $the_query->the_post();
		
		$content .= '<div class="widget-container" style="padding-top:15px;padding-bottom:15px;';
		if (!empty( $instance['post_separator_botom_border_color'] ) ) {
			$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<div class="row">';
		$content .= '<div class="col-xs-5">';
		$content .= '<div class="widget-image-container"';
		if ( !empty($instance['Thumbnail_height'] )) {
			$content .='style="height:'.$instance['Thumbnail_height'].'px">';
		}else{
			$content .= '>';
		}
		$content .= '<img class="widget-img img-responsive" src="'.wp_make_link_relative(get_the_post_thumbnail_url()).'">';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '<div class="col-xs-7" style="padding-left:0px">';
		$content .= '<div class="widget-title-continer">';
		$content .= '<p class="widget-time" style="margin-bottom: 10px;';
		if ( !empty($instance['post_date_color']) ) {
			$content .= 'color:#'.$instance['post_date_color'].';">';
		}else{
			$content .= '">';
		}
		$content .= '<span style="vertical-align: text-bottom;">
							<svg class="svg-icon" viewBox="0 0 20 20">
							<path fill="none" d="M16.254,3.399h-0.695V3.052c0-0.576-0.467-1.042-1.041-1.042c-0.576,0-1.043,0.467-1.043,1.042v0.347H6.526V3.052c0-0.576-0.467-1.042-1.042-1.042S4.441,2.476,4.441,3.052v0.347H3.747c-0.768,0-1.39,0.622-1.39,1.39v11.813c0,0.768,0.622,1.39,1.39,1.39h12.507c0.768,0,1.391-0.622,1.391-1.39V4.789C17.645,4.021,17.021,3.399,16.254,3.399z M14.17,3.052c0-0.192,0.154-0.348,0.348-0.348c0.191,0,0.348,0.156,0.348,0.348v0.347H14.17V3.052z M5.136,3.052c0-0.192,0.156-0.348,0.348-0.348S5.831,2.86,5.831,3.052v0.347H5.136V3.052z M16.949,16.602c0,0.384-0.311,0.694-0.695,0.694H3.747c-0.384,0-0.695-0.311-0.695-0.694V7.568h13.897V16.602z M16.949,6.874H3.052V4.789c0-0.383,0.311-0.695,0.695-0.695h12.507c0.385,0,0.695,0.312,0.695,0.695V6.874z M5.484,11.737c0.576,0,1.042-0.467,1.042-1.042c0-0.576-0.467-1.043-1.042-1.043s-1.042,0.467-1.042,1.043C4.441,11.271,4.908,11.737,5.484,11.737z M5.484,10.348c0.192,0,0.347,0.155,0.347,0.348c0,0.191-0.155,0.348-0.347,0.348s-0.348-0.156-0.348-0.348C5.136,10.503,5.292,10.348,5.484,10.348z M14.518,11.737c0.574,0,1.041-0.467,1.041-1.042c0-0.576-0.467-1.043-1.041-1.043c-0.576,0-1.043,0.467-1.043,1.043C13.475,11.271,13.941,11.737,14.518,11.737z M14.518,10.348c0.191,0,0.348,0.155,0.348,0.348c0,0.191-0.156,0.348-0.348,0.348c-0.193,0-0.348-0.156-0.348-0.348C14.17,10.503,14.324,10.348,14.518,10.348z M14.518,15.212c0.574,0,1.041-0.467,1.041-1.043c0-0.575-0.467-1.042-1.041-1.042c-0.576,0-1.043,0.467-1.043,1.042C13.475,14.745,13.941,15.212,14.518,15.212z M14.518,13.822c0.191,0,0.348,0.155,0.348,0.347c0,0.192-0.156,0.348-0.348,0.348c-0.193,0-0.348-0.155-0.348-0.348C14.17,13.978,14.324,13.822,14.518,13.822z M10,15.212c0.575,0,1.042-0.467,1.042-1.043c0-0.575-0.467-1.042-1.042-1.042c-0.576,0-1.042,0.467-1.042,1.042C8.958,14.745,9.425,15.212,10,15.212z M10,13.822c0.192,0,0.348,0.155,0.348,0.347c0,0.192-0.156,0.348-0.348,0.348s-0.348-0.155-0.348-0.348C9.653,13.978,9.809,13.822,10,13.822z M5.484,15.212c0.576,0,1.042-0.467,1.042-1.043c0-0.575-0.467-1.042-1.042-1.042s-1.042,0.467-1.042,1.042C4.441,14.745,4.908,15.212,5.484,15.212z M5.484,13.822c0.192,0,0.347,0.155,0.347,0.347c0,0.192-0.155,0.348-0.347,0.348s-0.348-0.155-0.348-0.348C5.136,13.978,5.292,13.822,5.484,13.822z M10,11.737c0.575,0,1.042-0.467,1.042-1.042c0-0.576-0.467-1.043-1.042-1.043c-0.576,0-1.042,0.467-1.042,1.043C8.958,11.271,9.425,11.737,10,11.737z M10,10.348c0.192,0,0.348,0.155,0.348,0.348c0,0.191-0.156,0.348-0.348,0.348s-0.348-0.156-0.348-0.348C9.653,10.503,9.809,10.348,10,10.348z"></path>
						</svg></span>';
		$content .= '<span style="padding-left:5px;vertical-align:text-top">'.get_the_date('d F Y').'</span>';
		$content .= '</p>';
		$content .= '<h4 style="margin-top: 0px;margin-bottom: 6px;text-transform: uppercase;">';
		$content .= '<a class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';

		if ( !empty($instance['title_color'] ) ) {
			$content .= 'style="color:#'.$instance['title_color'].'">'.get_the_title().'</a>';
		}else{
			$content .= '>'.get_the_title().'</a>';
		}
		if ( !empty($instance['show_comment_count'] ) ) {

			$style = '';
			if ( !empty($instance['comment_box_color'] ) ) {
				$style .= 'color:#'.$instance['comment_box_color'].';';
			}
			if ( !empty($instance['comment_box_background_color'] ) ) {
				$style .= 'background-color:#'.$instance['comment_box_background_color'].';';
			}
			$content .= '<span class="comment-color" style="'.$style.'">'.get_comments_number().'</span>';
		}

		$content .= '</h4>';
		$content .= '<p class="widget-time" style="margin-bottom: 10px;">';
		$content .= '<span style="vertical-align: text-bottom;">
		<svg class="svg-icon" viewBox="0 0 20 20">
							<path fill="none" d="M10.001,9.658c-2.567,0-4.66-2.089-4.66-4.659c0-2.567,2.092-4.657,4.66-4.657s4.657,2.09,4.657,4.657
							C14.658,7.569,12.569,9.658,10.001,9.658z M10.001,1.8c-1.765,0-3.202,1.437-3.202,3.2c0,1.766,1.437,3.202,3.202,3.202
							c1.765,0,3.199-1.436,3.199-3.202C13.201,3.236,11.766,1.8,10.001,1.8z"></path>
							<path fill="none" d="M9.939,19.658c-0.091,0-0.179-0.017-0.268-0.051l-7.09-2.803c-0.276-0.108-0.461-0.379-0.461-0.678
							c0-4.343,3.535-7.876,7.881-7.876c4.343,0,7.878,3.533,7.878,7.876c0,0.302-0.182,0.572-0.464,0.68l-7.213,2.801
							C10.118,19.64,10.03,19.658,9.939,19.658z M3.597,15.639l6.344,2.507l6.464-2.512c-0.253-3.312-3.029-5.927-6.404-5.927
							C6.623,9.707,3.848,12.326,3.597,15.639z"></path>
							<path fill="none" d="M9.939,19.658c0,0-0.003,0-0.006,0c-0.347-0.003-0.646-0.253-0.709-0.596L7.462,9.567
							C7.389,9.172,7.65,8.79,8.046,8.718C8.442,8.643,8.82,8.906,8.894,9.301l1.076,5.796l1.158-5.741
							c0.08-0.394,0.461-0.655,0.86-0.569c0.396,0.08,0.649,0.464,0.569,0.859l-1.904,9.427C10.585,19.413,10.286,19.658,9.939,19.658z"></path>
						</svg></span>';
		$content .= '<span style="padding-left:5px;text-transform: lowercase;vertical-align:text-top">'.strtolower(get_the_author()).'</span>';
		$content .= '</p>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		
		endwhile; 
		wp_reset_postdata(); 
	else:
		$content .= 'Sorry, no posts matched your criteria.';
	endif;
	return $content;
	}

	function simple_list_style($finalargs,$instance)
	{
		$content='';
		$the_query = new WP_Query( $finalargs );
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
			$content .= '<div class="widget-container" style="padding-top:5px;padding-bottom:5px;';
			if (!empty( $instance['post_separator_botom_border_color'] ) ) {
				$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
			}else{
				$content .= '">';
			}
			$content .= '<div class="row">';
			$content .= '<div class="col-xs-12">';
			$content .= '<h4 style="margin-top: 0px;margin-bottom: 6px;text-transform: uppercase;">';
			$content .= '<a class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';

			if ( !empty($instance['title_color'] ) ) {
				$content .= 'style="color:#'.$instance['title_color'].'">'.get_the_title().'</a>';
			}else{
				$content .= '>'.get_the_title().'</a>';
			}
			if ( !empty($instance['show_comment_count'] ) ) {

				$style = '';
				if ( !empty($instance['comment_box_color'] ) ) {
					$style .= 'color:#'.$instance['comment_box_color'].';';
				}
				if ( !empty($instance['comment_box_background_color'] ) ) {
					$style .= 'background-color:#'.$instance['comment_box_background_color'].';';
				}
				$content .= '<span class="comment-color" style="'.$style.'">'.get_comments_number().'</span>';
			}

			$content .= '</h4>';
			$content .= '<p class="widget-time" style="margin-bottom: 10px;';
			if ( !empty($instance['author_color'] ) ) {
				$content .= 'color:#'.$instance['author_color'].'">';
			}else{
				$content .= '">';
			}
			$content .= '<svg class="svg-icon" viewBox="0 0 20 20">
							<path fill="none" d="M10.001,9.658c-2.567,0-4.66-2.089-4.66-4.659c0-2.567,2.092-4.657,4.66-4.657s4.657,2.09,4.657,4.657
							C14.658,7.569,12.569,9.658,10.001,9.658z M10.001,1.8c-1.765,0-3.202,1.437-3.202,3.2c0,1.766,1.437,3.202,3.202,3.202
							c1.765,0,3.199-1.436,3.199-3.202C13.201,3.236,11.766,1.8,10.001,1.8z"></path>
							<path fill="none" d="M9.939,19.658c-0.091,0-0.179-0.017-0.268-0.051l-7.09-2.803c-0.276-0.108-0.461-0.379-0.461-0.678
							c0-4.343,3.535-7.876,7.881-7.876c4.343,0,7.878,3.533,7.878,7.876c0,0.302-0.182,0.572-0.464,0.68l-7.213,2.801
							C10.118,19.64,10.03,19.658,9.939,19.658z M3.597,15.639l6.344,2.507l6.464-2.512c-0.253-3.312-3.029-5.927-6.404-5.927
							C6.623,9.707,3.848,12.326,3.597,15.639z"></path>
							<path fill="none" d="M9.939,19.658c0,0-0.003,0-0.006,0c-0.347-0.003-0.646-0.253-0.709-0.596L7.462,9.567
							C7.389,9.172,7.65,8.79,8.046,8.718C8.442,8.643,8.82,8.906,8.894,9.301l1.076,5.796l1.158-5.741
							c0.08-0.394,0.461-0.655,0.86-0.569c0.396,0.08,0.649,0.464,0.569,0.859l-1.904,9.427C10.585,19.413,10.286,19.658,9.939,19.658z"></path>
						</svg>';
			$content .= '<span style="padding-left:5px;vertical-align:text-top; text-transform: lowercase;">'.strtolower(get_the_author()).'</span>';
			$content .= '</p>';
			$content .= '</div>';
			$content .= '</div>';
			$content .= '</div>';
			endwhile; 
		wp_reset_postdata(); 
		else:
			$content .= 'Sorry, no posts matched your criteria.';
		endif;
		return $content;
	}
	function minimal_list_style($finalargs,$instance)
	{
		$content = '';
		$the_query = new WP_Query( $finalargs );
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
		$content .= '<div class="widget-container" style="padding-top:10px;padding-bottom:10px;';
		if (!empty( $instance['post_separator_botom_border_color'] ) ) {
			$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<div class="row">';
		$content .= '<div class="col-xs-3">';
		$content .= '<div class="widget-left-date">';
		$content .= '<span>'.get_the_date('d').'</span>'.get_the_date('M').'';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '<div class="col-xs-9" style="padding-left:0px">';
		$content .= '<h4 style="margin-top: 0px;margin-bottom: 6px;text-transform: uppercase;">';
		$content .= '<a class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';

		if ( !empty($instance['title_color'] ) ) {
			$content .= 'style="color:#'.$instance['title_color'].'">'.get_the_title().'</a>';
		}else{
			$content .= '>'.get_the_title().'</a>';
		}
		if ( !empty($instance['show_comment_count'] ) ) {

			$style = '';
			if ( !empty($instance['comment_box_color'] ) ) {
				$style .= 'color:#'.$instance['comment_box_color'].';';
			}
			if ( !empty($instance['comment_box_background_color'] ) ) {
				$style .= 'background-color:#'.$instance['comment_box_background_color'].';';
			}
			$content .= '<span class="comment-color" style="'.$style.'">'.get_comments_number().'</span>';
		}

		$content .= '</h4>';
		$content .= '<p class="widget-time" style="margin-bottom: 10px;';
		if ( !empty($instance['author_color'] ) ) {
			$content .= 'color:#'.$instance['author_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<span class="">By</span>';
		$content .= '<span style="padding-left:5px;text-transform: lowercase;">'.strtolower(get_the_author()).'</span>';
		$content .= '</p>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		endwhile; 
		wp_reset_postdata(); 
		else:
			$content .= 'Sorry, no posts matched your criteria.';
		endif;
		return $content;
	}
	function simple_grid_style($finalargs,$instance)
	{
		$content = '';
		$the_query = new WP_Query( $finalargs );
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
		$content .= '<div class="widget-container" style="padding-top:10px;padding-bottom:10px;';
		if (!empty( $instance['post_separator_botom_border_color'] ) ) {
			$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<div class="row">';
		$content .= '<div class="col-xs-12" style="text-align: center;text-transform: capitalize;">';
				$content .= '<h4 style="margin-top: 0px;margin-bottom: 6px;">';
		$content .= '<a class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';

		if ( !empty($instance['title_color'] ) ) {
			$content .= 'style="color:#'.$instance['title_color'].'">'.get_the_title().'</a>';
		}else{
			$content .= '>'.get_the_title().'</a>';
		}
		if ( !empty($instance['show_comment_count'] ) ) {

			$style = '';
			if ( !empty($instance['comment_box_color'] ) ) {
				$style .= 'color:#'.$instance['comment_box_color'].';';
			}
			if ( !empty($instance['comment_box_background_color'] ) ) {
				$style .= 'background-color:#'.$instance['comment_box_background_color'].';';
			}
			$content .= '<span class="comment-color" style="'.$style.'">'.get_comments_number().'</span>';
		}

		$content .= '</h4>';
		$content .= '<p class="widget-time" style="margin-bottom: 10px;';
		if ( !empty($instance['author_color'] ) ) {
			$content .= 'color:#'.$instance['author_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<span class="">By</span>';
		$content .= '<span style="padding-left:5px;text-transform: lowercase;">'.strtolower(get_the_author()).'</span>';
		$content .= '</p>';
		$content .= '</div>';
		$content .= '</div>';
		
		$content .= '</div>';
		endwhile; 
		wp_reset_postdata(); 
		else:
			$content .= 'Sorry, no posts matched your criteria.';
		endif;
		return $content;
	}

	function clasiccoverstyle($finalargs,$instance)
	{	$content = '';
		$the_query = new WP_Query( $finalargs );
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
		$content .= '<div class="widget-container" style="padding-top:10px;padding-bottom:10px;';
		if (!empty( $instance['post_separator_botom_border_color'] ) ) {
			$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<div class="row">';
		$content .= '<div class="col-xs-12">';
		$content .= '<div class="widget-image-container"';
		
		if (!empty( $instance['Thumbnail_height'] ) ) {
			$content .= 'style="height:'.$instance['Thumbnail_height'].'px;" ';
		}else{
			$content .= 'style="height:150px;" ' ;
		}
		$content .= '>';
		$content .= '<img src="'.wp_make_link_relative(get_the_post_thumbnail_url()).'" class="widget-img" >';
		$content .= '<div class="dreming-overlay"></div>';
		$content .= '<div class="overlay-content">';
		$content .= '<a  class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'" ';
		if ( !empty($instance['title_color'] ) ) {
			$content .= 'style="color:#'.$instance['title_color'].'" ';
		}else{
			$content .= 'style="color:#fff;" ';
		}
		$content .= '>'.get_the_title().'</a>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		endwhile; 
		wp_reset_postdata(); 
		else:
			$content .= 'Sorry, no posts matched your criteria.';
		endif;
		return $content;
	}
	function moderncoverstyle($finalargs,$instance)
	{   $content = '';
		$the_query = new WP_Query( $finalargs );
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
		$content .= '<div class="widget-container" style="padding-top:10px;padding-bottom:10px;';
		if (!empty( $instance['post_separator_botom_border_color'] ) ) {
			$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<div class="row">';
		$content .= '<div class="col-xs-12" >';
		$content .= '<div class="widget-image-container" style="height: 150px">';
		$content .= '<img src="'.wp_make_link_relative(get_the_post_thumbnail_url()).'" class="widget-img" >';
		$content .= '</div>';
		$content .= '<div class="overlay-content">';
		$content .= '<a  class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';
		if ( !empty($instance['title_color'] ) ) {
		$content .= 'style="color:#'.$instance['title_color'].';background:rgba(0, 0, 0, 0.4);font-weight:600;padding:3px">';
		}else{
		$content .= ' style="color:#fff;background:rgba(0, 0, 0, 0.4);font-weight:600;padding:3px">';
		}

		$content .= get_the_title();
		$content .= '</a>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		endwhile; 
		wp_reset_postdata(); 
		else:
			$content .= 'Sorry, no posts matched your criteria.';
		endif;
		return $content;
	}
	function cleancoverstyle($finalargs,$instance)
	{	$content = '';
		$the_query = new WP_Query( $finalargs );
		if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post();
		$content .= '<div class="widget-container" style="padding-top:10px;padding-bottom:10px;';
		if (!empty( $instance['post_separator_botom_border_color'] ) ) {
			$content .= 'border-bottom:1px dashed  #'.$instance['post_separator_botom_border_color'].'">';
		}else{
			$content .= '">';
		}
		$content .= '<div class="row">';
		$content .= '<div class="col-xs-12" >';
			$content .= '<div class="widget-image-container" style="height: 150px">';
				$content .= '<img src="'.wp_make_link_relative(get_the_post_thumbnail_url()).'" class="widget-img" >';
			$content .= '</div>';
			$content .= '<div class="overlay-content-top">';
			$content .= '<a  class="widget-anchor" href="'.wp_make_link_relative(get_post_permalink()).'"';
			if (!empty($instance['title_color'])) {
				$content .= 'style="color:#'.$instance['title_color'].'">';
			}else{
				$content .= '>';
			}
			 
			$content .= get_the_title();
			$content .= '</a>';
			$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
		endwhile; 
		wp_reset_postdata(); 
		else:
			$content .= 'Sorry, no posts matched your criteria.';
		endif;
		return $content;
	}
?>
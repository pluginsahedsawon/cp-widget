<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              inserttech.com/sahed.sawon
 * @since             1.0.0
 * @package           Widgetsbundle
 *
 * @wordpress-plugin
 * Plugin Name:       widgetsbundle
 * Plugin URI:        https://codex.wordpress.org/Plugins/widgetsbundle
 * Description:       amazing wordpress post widget bundle including any custom post.
 * Version:           1.0.0
 * Author:            sahed sawon
 * Author URI:        inserttech.com/sahed.sawon
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       widgetsbundle
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-widgetsbundle-activator.php
 */
function activate_widgetsbundle() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-widgetsbundle-activator.php';
	Widgetsbundle_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-widgetsbundle-deactivator.php
 */
function deactivate_widgetsbundle() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-widgetsbundle-deactivator.php';
	Widgetsbundle_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_widgetsbundle' );
register_deactivation_hook( __FILE__, 'deactivate_widgetsbundle' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-widgetsbundle.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_widgetsbundle() {

	$plugin = new Widgetsbundle();
	$plugin->run();

}
run_widgetsbundle();
